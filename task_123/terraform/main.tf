# описываем провайдера
terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.5.0"
    }
  }
}

# описываем провайдера
provider "google" {
  credentials = file("~/gcp_key/def_terraform-project-334004-9972ff461c4a.json") # креды для доступа к облаку

  project = "terraform-project-334004" # project_id
  region  = "us-east1" # регион развертывания
}


// генерим имя инстанса
resource "random_id" "instance_id" {
 byte_length = 8
}

// создаем инстанс
resource "google_compute_instance" "default" {
 name         = "xxxxx-${random_id.instance_id.hex}"
 machine_type = "e2-micro"
 zone         = "us-east1-b"
 tags = ["web","http-server"]
 
 boot_disk {
   initialize_params {
     image = "ubuntu-2004-focal-v20200720"
   }
 }

metadata = {
   ssh-keys = "ubuntu:${file("~/gitlab/gitlab.pub")}"
 }

 network_interface {
   network = "default"

   access_config {
     // добавляем эту секцию, чтобы у инстанса был публичный IP
   }
 }
}

// выводим публичный IP инстанса
output "ip" {
 value = google_compute_instance.default.network_interface.0.access_config.0.nat_ip
}

// добавляем правила в файрвол
resource "google_compute_firewall" "default" {
 name    = "web-firewall"
 network = "default"

 allow {
   protocol = "icmp"
 }

  allow {
    protocol = "tcp"
    ports    = ["80", "22", "9090", "3000"]
  }

 source_ranges = ["0.0.0.0/0"]
 target_tags = ["web"]
}
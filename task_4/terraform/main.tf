provider "aws" {
  region = "us-east-1"
}

# Ищем образ с последней версией Ubuntu
data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

# Добавляем ключ пару
resource "aws_key_pair" "my_key_pair" {
  key_name   = "gitlab"
  public_key = file("/home/awam/gitlab/gitlab.pub")
}

# resource "aws_eip" "my_static_ip" {
#   instance = aws_instance.my_webserver[count.index]
#   tags = {
#     Name  = "Web Server IP"
#   }
# }

# Запускаем инстанс
resource "aws_instance" "my_webserver" {
  for_each = {
    web = { type = "t2.micro", public_ip = true },
    db  = { type = "t2.micro", public_ip = true },
    mon = { type = "t2.micro", public_ip = true }
  }
  
  ami                    = data.aws_ami.ubuntu.id
  instance_type = each.value["type"]
  vpc_security_group_ids = [aws_security_group.my_webserver.id]
  associate_public_ip_address = each.value["public_ip"]
  key_name = aws_key_pair.my_key_pair.key_name
  tags = {
    Name = each.key
  }

  lifecycle {
    create_before_destroy = true
  }

}

resource "aws_security_group" "my_webserver" {
  name        = "Servers Security Group"
  description = "Security group for accessing traffic to our ReactJS Server"


  dynamic "ingress" {
    for_each = ["80", "22", "9100", "9090", "3000"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

# разрешаем весь исходящий трафик

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Web Server SecurityGroup"
  }
}
